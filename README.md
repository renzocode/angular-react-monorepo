# angular-react-monorepo

### NX 

Install nx command


```bash
$ npm i -g nrwl/cli 
```
linking nx to bin

```bash
$ sudo ln -s /usr/local/lib/nodejs/node-v12.18.4-linux-x64/bin/nx /usr/bin/nx
```

create a nx project

link

https://duncanhunter.gitbook.io/enterprise-angular-applications-with-ngrx-and-nx/introduction/2-creating-an-nx-workspace

```bash
$ npx create-nx-workspace nx-angular
```
install @nrwl/schematics

```bash
$ npm i @nrwl/schematics
```

add  angular cli

```bash
$ nx generate @nrwl/angular:application bussiness
```

create an angular-react project with nx 2

```bash
$ npx --ignore-existing create-nx-workspace angular-react --preset=angular
```

```vim
  ? Application name  angular-react-monorepo
  
  ? Default stylesheet format 
  CSS 
❯ SASS(.scss)  [ http://sass-lang.com   ] 
  Stylus(.styl)[ http://stylus-lang.com ] 
  LESS         [ http://lesscss.org     ] 

  ❯ TSLint [ Used by Angular CLI ]

  ? Use Nx Cloud? (It's free and doesn't require registration.) (Use arrow keys)
  Yes [Faster builds, run details, Github integration. Learn more at https://nx.app] 
❯ No
```

go to into to the angular-react
```bash
$ cd angular-react
```
Add Angular Capabilities

```bash
$ ng add @nrwl/angular
```

```bash
? Which Unit Test Runner would you like to use for the application? (Use arrow keys)
❯ Jest  [ https://jestjs.io ] 
  Karma [ https://karma-runner.github.io ] 

? Which E2E Test Runner would you like to use? (Use arrow keys)
❯ Cypress    [ https://www.cypress.io ] 
  Protractor [ https://www.protractortest.org ] 
```
Creating an Angular Application

```bash
$ ng generate application my-other-angular-app
```
create a module and routing

```bash
$ ng g module user --routing
```

create a component

```bash
$ ng g component user
```
create a library

```bash
$ ng generate library my-lib
```

add React capabilities

```bash
$ ng add @nrwl/react
```

creating a React Application

```bash
$ ng g @nrwl/react:app reactapp
```

web component in React and Angular

link

https://indepth.dev/how-to-talk-with-web-components-in-react-and-angular/

## Install GitLab on Docker

Add environment variable to .profile

```bash
export GITLAB_HOME=/srv/gitlab
```

Reload profile

```
$ source ~/.profile
```

Install Gitlab using Docker Engine

```bash
$ docker run --detach \
  --hostname ip \
  --publish 443:443 --publish 80:80 --publish 22:22 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
  gitlab/gitlab-ee:latest
```

note:
  ip = 172.17.0.2

The initialization process

```bash
$ docker logs -f gitlab
```

500 Internal Error

```bash
$ docker restart gitlab
```

Enter running container or Image base

```bash
docker exec -it gitlab /bin/bash
```
add ssh key in gitlab-runner server

gitlab-runner problem

```bash
$ sudo gitlab-runner run
```

```bash
  Runtime platform                                    arch=amd64 os=linux pid=14588 revision=e95f89a0 version=13.4.1
  Starting multi-runner from /etc/gitlab-runner/config.toml...  builds=0
  Running in system-mode.                            
                                                   
  Configuration loaded                                builds=0
  listen_address not defined, metrics & debug endpoints disabled  builds=0
  [session_server].listen_address not defined, session endpoints disabled  builds=0
  ERROR: Checking for jobs... forbidden               runner=UzMj-j7F
  ERROR: Checking for jobs... forbidden               runner=W5QJsiJN
  ERROR: Checking for jobs... forbidden               runner=FPR8Frhr
  ERROR: Checking for jobs... forbidden               runner=xfFshEWn
```

solved

```bash
$ sudo gitlab-runner --debug verify
```

```bash
  Runtime platform                                    arch=amd64 os=linux pid=14615 revision=e95f89a0 version=13.4.1
  Checking runtime mode                               GOOS=linux uid=0
  Running in system-mode.                            
                                                   
  Dialing: tcp 172.17.0.2:80 ...                     
  ERROR: Verifying runner... is removed               runner=UzMj-j7F
  Dialing: tcp 172.17.0.2:80 ...                     
  ERROR: Verifying runner... is removed               runner=W5QJsiJN
  Dialing: tcp 172.17.0.2:80 ...                     
  ERROR: Verifying runner... is removed               runner=FPR8Frhr
  Dialing: tcp 172.17.0.2:80 ...                     
  ERROR: Verifying runner... is removed               runner=xfFshEWn
  FATAL: Failed to verify runners                    
```

```bash
$ gitlab-runner verify --delete
```

```bash
  Runtime platform                                    arch=amd64 os=linux pid=14766 revision=e95f89a0 version=13.4.1
  WARNING: Running in user-mode.                     
  WARNING: The user-mode requires you to manually start builds processing: 
  WARNING: $ gitlab-runner run                       
  WARNING: Use sudo for system-mode:                 
  WARNING: $ sudo gitlab-runner...                   
                                                   
  ERROR: Verifying runner... is removed               runner=yGnzgNM5
  ERROR: Verifying runner... is removed               runner=fcKged4E
  ERROR: Verifying runner... is removed               runner=wy5zNBKg
  Updated /home/renzo/.gitlab-runner/config.toml     
```

```bash
$ sudo gitlab-runner --debug verify
```

```bash
  Runtime platform                                    arch=amd64 os=linux pid=14813 revision=e95f89a0 version=13.4.1
  Checking runtime mode                               GOOS=linux uid=0
  Running in system-mode.
```
how to run gitlab-runner

moving to into of docker

```bash
$ docker run -p 8000:8000 -it id /bin/bash
```
In gitlab account

you should create a repository without README.md

"The repository for this project is empty"

open terminal

create a monorepo project
```bash
$ npx create-nx-workspace repository-name
```
```bash
  npx: installed 179 in 14.88s
  ? What to create in the new workspace empty             [an empty workspace with a layout that works best for building apps]
  ? CLI to power the Nx workspace       Nx           [Recommended for all applications (React, Node, etc..)]
  ? Use Nx Cloud? (It's free and doesn't require registration.) No
  Creating a sandbox with Nx...
```
```bash
$ cd repository-name
```
Install the Angular schematic needed to create an Angular app.
```bash
$ npm install @nrwl/angular
```
Create a new app by running the below command in the terminal in a directory of your choice.  Choose SASS as your stylesheet language

```bash
$ nx generate @nrwl/angular:app app-name --routing
```
Run the app

```bash
$ nx serve customer-portal
```

create a directory

```bash
$ mkdir /root/Downloads/Projects/repository-name/
```
```bash
$ cd /root/Downloads/Projects/repository-name/
```

gitlab-runner register

```bash
$ gitlab-runner register
```

```bash
root@abf615b730eb:~/Downloads/Projects/nx-angular# gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=176 revision=e95f89a0 version=13.4.1
Running in system-mode.                            
                                                   
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
http://172.17.0.2/ (ip)
Please enter the gitlab-ci token for this runner:
3dFZvn8sTkVpER3o5iyx (token)
Please enter the gitlab-ci description for this runner:
[abf615b730eb]: nx-angular (repository-name)
Please enter the gitlab-ci tags for this runner (comma separated):
(enter)
Registering runner... succeeded                     runner=3dFZvn8s
Please enter the executor: parallels, ssh, virtualbox, docker+machine, docker-ssh+machine, custom, docker, docker-ssh, shell, kubernetes:
shell (executor)
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded! 

```
run gitlab-runner

```bash
$ gitlab-runner run
```

how to symlink nx-angular index to nginx

```bash
$ cd /build/partial-token/0/root/repository-name/
```

```bash
$ pwd
```
ex 
```bash
/root/Downloads/Projects/nx-angular/builds/KKhyVPN8/0/root/nx-angular
```

### Dockerfile

create a Dockerfile file.

```bash
$ nvim Dockerfile
```

Build Dockerfile

https://nodejs.org/dist/v12.19.0/node-v12.19.0-linux-x64.tar.xz

```bash
$ docker build -t  debian27/nx-angular .
```
Run Docker
```bash
$ docker run debian27/nx-angular
```

Push to Docker Hub

```bash
$ docker push debian27/nx-angular
```

Pull from Docker Hub

```bash
$ docker pull debian27/nx-angular
```

Moving into

```bash
$ docker run -p 8000:8000 -it id /bin/bash
```
Exec container

```bash
$ docker exec -it web
```
Delete container 

```bash
$ docker container ls -a
```

```bash
$ docker container rm id
```

```bash
$ docker images
```

```bash
$ docker image rm id // or docker image -f id
```

