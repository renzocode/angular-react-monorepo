import { Component } from '@angular/core';

@Component({
  selector: 'angular-react-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'angular-react-monorepo';
}
