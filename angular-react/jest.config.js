module.exports = {
  projects: [
    '<rootDir>/apps/angular-react-monorepo',
    '<rootDir>/libs/domain',
    '<rootDir>/libs/catalog/api',
    '<rootDir>/libs/ordering/shell',
    '<rootDir>/libs/catalog/domain',
    '<rootDir>/libs/catalog/shell',
    '<rootDir>/libs/shared/util-auth',
    '<rootDir>/libs/shared/ui-address',
    '<rootDir>/libs/catalog/feature',
    '<rootDir>/libs/catalog/data-access',
    '<rootDir>/libs/validator/domain',
    '<rootDir>/libs/validator/util-auth',
  ],
};
