import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogComponent } from './catalog/catalog.component';

@NgModule({
  imports: [CommonModule],
  declarations: [CatalogComponent],
})
export class CatalogApiModule {}
